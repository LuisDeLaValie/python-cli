from pathlib import Path
import subprocess


def crearDirectorio(path):
    # Utiliza el método mkdir() de la clase Path para crear el directorio
    Path(path).mkdir()
    


def commandsScrip(command):
    # Ejecutar el comando Bash desde Python
    resultado = subprocess.run(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)

    # comprobar si existe un error
    if resultado.returncode != 0  :
        raise Exception(resultado.stderr)
    
    # Imprimir la salida y los errores (si los hay)
    return resultado.stdout

def sobreEscribirArchivos(nombre_archivo,texto_a_reemplazar,nuevo_texto):
    with open(nombre_archivo, 'r') as archivo:
        contenido = archivo.read()

    # Realizar la sustitución del texto
    contenido_modificado = contenido.replace(texto_a_reemplazar, nuevo_texto)

    # Escribir el contenido modificado de vuelta al archivo
    with open(nombre_archivo, 'w') as archivo:
        archivo.write(contenido_modificado)

def existeTextoArchivo(archivo, texto_a_buscar):
    try:
        with open(archivo, 'r') as file:
            contenido = file.read()
            return texto_a_buscar in contenido
    except FileNotFoundError:
        print(f"El archivo '{archivo}' no existe.")
        return False
    except Exception as e:
        print(f"Error al leer el archivo: {e}")
        return False
