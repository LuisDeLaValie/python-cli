import click

from cmd.go.golang import go, initGo

@click.group()
def cmd():
    pass

def initCMD():
    cmd.add_command(go)
    initGo()

    cmd()