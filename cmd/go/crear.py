
import sys
import click

from utils.utils import crearDirectorio


@click.group(help='Crear un nuevo proyecto en Go')
def create():
    pass

@create.command(help='Crear un nuevo proyecto api en Go')
@click.option('-a', '--arquitectura', default="CA", help='Tipo de arquitectura [DD => Domain Driver, CA => Clean Architecture]')
@click.argument('nombre')
def api(nombre, arquitectura):
    try:
        
        if not arquitectura in ['DD','CA']:
            raise click.BadParameter(f"No existe la arquitectura {arquitectura} asegurate de que sea uno de estos [DD, CA]")
        

        directorio={
            'CA':[
                    f"{nombre}/cmd",
                    f"{nombre}/internal",
                    f"{nombre}/docs",
                    f"{nombre}/internal/app",
                    f"{nombre}/internal/config",
                    f"{nombre}/internal/framework",
                ]
        }
        crearDirectorio(directorio[arquitectura])
        click.echo(directorio[arquitectura])
    except click.BadParameter as e:
        click.echo(e,err=True)
        sys.exit(1) 

@create.command(help="No disponible")
@click.option('-a', '--arquitectura', default="CA", help='Tipo de arquitectura [DD => Domain Driver, CA => Clean Architecture]')
@click.argument('nombre')
def web(nombre, arquitectura):
    click.echo("No disponible")


@create.command(help="No disponible")
@click.option('-a', '--arquitectura', default="CA", help='Tipo de arquitectura [DD => Domain Driver, CA => Clean Architecture]')
@click.argument('nombre')
def cmd(nombre, arquitectura):
    click.echo("No disponible")


def initGoCreate():
    pass