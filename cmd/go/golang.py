import os
import sys
import click
from cmd.go.crear import create, initGoCreate

from utils.utils import commandsScrip, existeTextoArchivo, sobreEscribirArchivos


@click.group(help='CLI para la creacion de proyecto en Go')
def go():    
    pass


@go.command()
def add():
    click.echo("Este es el subcomando 2")


@go.command(help="Instalara una nueva version de Go, por defecto es la version 1.21.6")
@click.option('-v', '--version', default="1.21.6", help='Version de Go a instalar Puedes ver las demas verificacion en https://go.dev/dl/')
def install(version):
    try:
        #  Descargar archivo tar de go
        click.echo("### Descargando Go ###################")
        commandsScrip(f"curl -LO https://go.dev/dl/go{version}.linux-amd64.tar.gz")
        
        #  Inctalr go en /usr/local
        click.echo("### Installando Go ###################")
        if os.path.exists("/usr/local/go"):        
            commandsScrip(f"sudo rm -rf /usr/local/go && tar -C /usr/local -xzf go{version}.linux-amd64.tar.gz")
        else:
            commandsScrip(f"sudo tar -C /usr/local -xzf go{version}.linux-amd64.tar.gz")

        # Configurar entorgo de go
        click.echo("### Configurando Go ##################")
        filezshrc=os.path.expanduser('~/.zshrc')
        if not existeTextoArchivo(filezshrc,"export GOROOT=/usr/local/go"):
            oldtext="""
#************************************************************************************************
#***  Entorno de desarrollo  ********************************************************************
#************************************************************************************************
"""
            newtwxt=f"{oldtext}\nexport GOROOT=/usr/local/go"
            sobreEscribirArchivos(filezshrc,oldtext,newtwxt)
        
        if not existeTextoArchivo(filezshrc,"export PATH=$PATH:/usr/local/go/bin"):
            oldtext="export GOROOT=/usr/local/go"
            newtwxt=f"{oldtext}\nexport PATH=$PATH:/usr/local/go/bin"   
            sobreEscribirArchivos(filezshrc,oldtext,newtwxt)
        
        if not existeTextoArchivo(filezshrc,"export GOPATH=~/lib/go"):
            oldtext="export PATH=$PATH:/usr/local/go/bin"
            newtwxt=f"{oldtext}\nexport GOPATH=~/lib/go"   
            sobreEscribirArchivos(filezshrc,oldtext,newtwxt)
        
        if not existeTextoArchivo(filezshrc,"export GOBIN=~/lib/go/bin"):
            oldtext="export GOPATH=~/lib/go"
            newtwxt=f"{oldtext}\nexport GOBIN=~/lib/go/bin\n"   
            sobreEscribirArchivos(filezshrc,oldtext,newtwxt)
        
        # eliminar archivo tar
        commandsScrip(f"rm go1.21.6.linux-amd64.tar.gz")
        

    except Exception as e:
        click.echo(e,err=True)
        sys.exit(1) 

def initGo():
    go.add_command(create)
    initGoCreate()