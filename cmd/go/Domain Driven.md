Al organizar una API REST en Go que sigue la arquitectura de Domain-Driven Design (DDD), puedes estructurar tus carpetas de manera que refleje la separación de las capas definidas por DDD. A continuación, te proporciono una estructura de carpetas básica que puedes adaptar según tus necesidades específicas:

```plaintext
/myapp
|-- /cmd
|   `-- main.go
|-- /internal
|   |-- /app
|   |   |-- /handler      // Controladores HTTP
|   |   |   `-- user_handler.go
|   |   |-- /service      // Servicios de aplicación
|   |   |   `-- user_service.go
|   |   |-- /repository   // Repositorios de DDD
|   |   |   `-- user_repository.go
|   |   |-- /dto          // Objetos de transferencia de datos
|   |   |   `-- user_dto.go
|   |   |-- /entity       // Entidades de dominio
|   |   |   `-- user.go
|   |   |-- /valueobject  // Objetos de valor
|   |   |   `-- email.go
|   |   |-- /event        // Eventos de dominio
|   |   |   `-- user_event.go
|   |   `-- /errors       // Errores de dominio
|   |       `-- app_errors.go
|   |-- /config           // Configuraciones de la aplicación
|   |   `-- app_config.go
|   |-- /domain           // Lógica de dominio
|   |   `-- user.go
|   `-- /infrastructure   // Código de infraestructura
|       |-- /persistence  // Implementación de repositorios
|       |   `-- mysql_user_repository.go
|       |-- /transport    // Implementación de controladores
|           `-- http_user_controller.go
|-- /migrations           // Scripts de migración de base de datos
|-- /scripts              // Scripts útiles para el desarrollo, pruebas, etc.
|-- /pkg                  // Paquetes compartidos entre aplicaciones
|-- /api                  // Archivos OpenAPI, Swagger, etc.
|-- /docs                 // Documentación
|-- go.mod
|-- go.sum
|-- README.md
```

En esta estructura:

- **cmd**: Contiene el punto de entrada de la aplicación, como el archivo `main.go`.
- **internal**: Es donde resides la implementación específica de tu aplicación.
  - **app**: Contiene los componentes de la aplicación, como controladores HTTP, servicios de aplicación, repositorios, DTOs, etc.
  - **config**: Configuraciones específicas de la aplicación.
  - **domain**: Contiene lógica de dominio pura, como las definiciones de las entidades, objetos de valor, eventos, etc.
  - **infrastructure**: Contiene la implementación de detalles de infraestructura, como persistencia y transporte.
- **migrations**: Scripts de migración de base de datos.
- **scripts**: Scripts útiles para tareas de desarrollo, pruebas, etc.
- **pkg**: Paquetes compartidos que pueden ser utilizados por otras aplicaciones.
- **api**: Archivos relacionados con la especificación de la API, como archivos OpenAPI o Swagger.
- **docs**: Documentación del proyecto.
- **go.mod, go.sum**: Archivos de gestión de módulos de Go.

Esta estructura es solo un punto de partida y puede modificarse según las necesidades específicas de tu aplicación y equipo. Además, ten en cuenta que DDD es un enfoque flexible y, en algunos casos, puede tener variaciones según la interpretación y las necesidades del proyecto.